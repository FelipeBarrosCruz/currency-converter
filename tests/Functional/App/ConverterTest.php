<?php

namespace Tests\Functional\App;

class ConverterTest extends BaseTestCase
{

    public function testPostConvertWithBodyEmpty()
    {
        $response = $this->runApp('POST', '/converter');
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The body cannot be empty');
    }

    public function testPostConvetWithInvalidFromField()
    {
        $postData = [
            'currency' => 'ARN',
            'to' => 'USD',
            'amount' => 1
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The From is required');
    }

    public function testPostConvetWithInvalidToField()
    {
        $postData = [
            'from' => 'USD',
            'currency' => 'USD',
            'amount' => 1
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The To is required');
    }


    public function testPostConvetWithInvalidCurrenciesValue()
    {
        $postData = [
            'from' => 'USD',
            'to' => 'USD',
            'amount' => 1
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The currencies cannot be equals');
    }

    public function testPostConvetWithInvalidAmountField()
    {
        $postData = [
            'from' => 'USD',
            'to' => 'USD'
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The Amount is required');
    }

    public function testPostConvetWithInvalidAmountValue()
    {
        $postData = [
            'from' => 'USD',
            'to' => 'USD',
            'amount' => 0
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 400);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'error', $body);
        $this->assertEquals($body['status'], false);
        $this->assertEquals($body['error'], 'The Amount minimum is 1');
    }

    public function testPostConveterAndReturnsAValidAmount()
    {
        $postData = [
            'from' => 'USD',
            'to' => 'BRL',
            'amount' => 1
        ];
        $response = $this->runApp('POST', '/converter', $postData);
        $body = $this->getParsedResponseBodyToJson($response);
        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertArrayHasKey('status', $body);
        $this->assertArrayHasKey( 'amount', $body);
        $this->assertEquals($body['status'], true);
        $this->assertGreaterThan(0, $body['amount']);
    }
}