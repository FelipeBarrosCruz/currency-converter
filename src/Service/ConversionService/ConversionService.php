<?php
namespace CurrencyConversion\Service\ConversionService;


use CurrencyConversion\DTO\ConversionDTO;
use GuzzleHttp\Client;

class ConversionService implements ConversionServiceInterface
{
    private $conversionDTO;
    private $client;
    private $rate;

    /**
     * ConversionService constructor.
     * @param ConversionDTO $conversionDTO
     */
    public function __construct(ConversionDTO $conversionDTO)
    {
        $this->conversionDTO = $conversionDTO;
        $this->client = new Client();
    }

    /**
     * @param string $conversionKey
     * @return string
     */
    private function buildApiUrl(string $conversionKey)
    {
        return "https://free.currencyconverterapi.com/api/v6/convert?q={$conversionKey}&compact=y";
    }

    /**
     * @throws InvalidArgumentException
     */
    private function checkHasRateSetup()
    {
        if (is_null($this->rate)) {
            throw new \InvalidArgumentException('The rate is not setup');
        }
    }

    /**
     * @return ConversionServiceInterface
     */
    public function convert(): ConversionServiceInterface
    {
        $conversionKey = $this->conversionDTO->from . '_' . $this->conversionDTO->to;
        $response = $this->client->get($this->buildApiUrl($conversionKey));
        $body = json_decode((string) $response->getBody(), true);
        $this->rate = $body[$conversionKey]['val'];
        return $this;
    }

    /**
     * @return float
     * @throws InvalidArgumentException
     */
    public function getRate(): float
    {
        $this->checkHasRateSetup();
        return $this->rate;
    }

    /**
     * @return float|int
     * @throws InvalidArgumentException
     */
    public function getAmountConverted(): float
    {
        $this->checkHasRateSetup();
        return $this->conversionDTO->amount * $this->rate;
    }
}