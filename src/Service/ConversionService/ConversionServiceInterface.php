<?php
namespace CurrencyConversion\Service\ConversionService;

interface ConversionServiceInterface
{
    public function convert(): ConversionServiceInterface;
    public function getRate(): float;
    public function getAmountConverted(): float;
}